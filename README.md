Color UI 我想大家都知晓吧，我就不过多阐述了，是 **文晓港** 大佬开发的一款适应于H5、微信小程序、安卓、ios、支付宝的高颜值，高度自定义的 Css 组件库.，属于出道即巅峰的史诗级大作，众所周知，万物皆可 Color UI，很多人用 Color UI 做了不少精美的项目，我也一样，在此再次感谢作者。

本人自己经常使用 Color UI，但是又找不到一个满意的文档，不如自己搭建一个文档，自己使用方便的同时也方便大家使用，这应该是全网最全的Color UI 使用文档了，建议大家收藏。

[ColorUI 使用文档地址](https://miren.lovemi.ren/colorui-document/)

[ColorUI Github地址](https://github.com/weilanwl/ColorUI)

[ColorUI 插件市场](https://ext.dcloud.net.cn/plugin?id=239)

[ColorUI 在线展示](https://miren.lovemi.ren/colorui-h5/h5/#/) 因为右侧的h5是用iframe来嵌入的，因此字体和极小一部分的样式会有出入，文档里只做展示，比较完整的样式，点这里查看。